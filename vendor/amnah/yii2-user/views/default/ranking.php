<?php

use yii\helpers\Html;
use yii\grid\GridView;
$user = \Yii::$app->getModule("user")->model("User");
$role = \Yii::$app->getModule("user")->model("Role");

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var amnah\yii2\user\models\User $user
 * @var amnah\yii2\user\models\Role $role
 * @var amnah\yii2\user\models\search\UserSearch $searchModel
 */

$this->title = 'Ranking';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/user/ranking']];
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php if (!defined('IS_ARCHIVE') && false) : ?>
        <div class="alert alert-block alert-info">
            Mọi thông tin về kết quả của vòng loại vui lòng xem lại <a target="_blank" href="<?= Yii::$app->homeUrl; ?>archive.php/user/default/ranking">tại đây</a>.
        </div>
    <?php endif; ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'full_name',
            'email',
            'bet_times',
            'win_times',
            [
                'attribute' => 'win_rate',
                'label' => 'Win Rate (%)'
            ],
            [
                'attribute' => 'money',
                'label' => 'Current Money'
            ],
            [
                'attribute' => 'total_money',
                'value' => function($model, $index, $dataColumn) {
                        return is_null($model['total_money']) ? 0 : $model['total_money'];
                    }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{history}',
                'headerOptions' => [
                    'width' => '25'
                ],
                'buttons' => [
                    'history' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-list"></span>', array('/bet/view-by-user', 'id' => $model['id']), [
                                'title' => Yii::t('app', 'Bets History'),
                            ]);
                        },
                    ],
                'visible' => !Yii::$app->user->isGuest
            ]
        ],
    ]); ?>

</div>
