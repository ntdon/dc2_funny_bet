<?php

namespace amnah\yii2\user\models\forms;

use amnah\yii2\user\models\User;
use Yii;
use yii\base\Model;

/**
 * Forgot password form
 */
class BonusMoneyForm extends Model {

    public $bonus_money;

    /**
     * @return array the validation rules.
     */
    public function rules() {
        return [
            ["bonus_money", "required"],
            ["bonus_money", "integer", "min" => 10],
            ["bonus_money", "isDivisibleByTen"],
        ];
    }

    public function isDivisibleByTen($attribute) {
        if (($this->$attribute % 10) != 0) {
            $this->addError($attribute, 'must be multiple of 10');
        }
    }

    public function updateMoney()
    {
        if ($this->validate())
        {
            foreach (User::findAll(['status' => 1]) as $user) {
                $user->profile->money += $this->bonus_money;
                $user->profile->save(false, ['money']);
            }
            return true;
        }

        return false;
    }

}
